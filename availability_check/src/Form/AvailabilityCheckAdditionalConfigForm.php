<?php

namespace Drupal\availability_check\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the main configuration form for the availability check module.
 */
class AvailabilityCheckAdditionalConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'availability_check_additional_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'availability_check.additional_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('availability_check.additional_settings');
    $values = unserialize($config->get('values'));
    $saved_count = count($values);
    $options = ['min' => t('Minute'), 'hour' => t('Hour'), 'day' => t('Day'), 'week' => t('Week')];

    if (!isset($_POST['config_container'])) {
      $count = $saved_count;
      $form_state->set('count', $saved_count);
    }
    else {
      $count = $form_state->get('count');
      if ($count === NULL) {
        $input = $form_state->getUserInput();
        $count = count($input['config_container']['config_fieldset']);
        $form_state->set('count', $count);
      }
    }

    $form['#tree'] = TRUE;
    $form['config_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="config-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $count; $i++) {
      $form['config_container']['config_fieldset'][$i] = [
        '#type' => 'fieldset',
        '#title' => $this->t(''),
      ];
      $form['config_container']['config_fieldset'][$i]['domain'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Domain'),
        '#default_value' => $values[$i]['domain'][$i],
        '#required' => TRUE,
      ];
      $form['config_container']['config_fieldset'][$i]['check_period'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Check period'),
        '#default_value' => $values[$i]['check_period'][$i],
        '#required' => TRUE,
      ];
      $form['config_container']['config_fieldset'][$i]['time_rate'][$i] = [
        '#type' => 'select',
        '#title' => t(''),
        '#options' => $options,
        '#default_value' => $values[$i]['time_rate'][$i],
        '#required' => TRUE,
      ];
      $form['config_container']['config_fieldset'][$i]['user'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('User'),
        '#default_value' => $values[$i]['user'][$i],
        '#required' => TRUE,
      ];
      $form['config_container']['config_fieldset'][$i]['pass'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('User pass'),
        '#default_value' => $values[$i]['pass'][$i],
        '#required' => TRUE,
      ];
      $form['config_container']['config_fieldset'][$i]['mattermost_channel'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Mattermost channel'),
        '#default_value' => $values[$i]['mattermost_channel'][$i],
        '#required' => TRUE,
      ];
      $form['config_container']['config_fieldset'][$i]['token_of_domain'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Token of domain'),
        '#default_value' => $values[$i]['token_of_domain'][$i],
      ];
      $form['config_container']['config_fieldset'][$i]['enable_availability_check'][$i] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Availability check'),
        '#default_value' => $values[$i]['enable_availability_check'][$i],
      ];
    }

    $form['config_container']['actions'] = [
      '#type' => 'actions',
    ];
    $form['config_container']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'config-fieldset-wrapper',
      ],
    ];
    if ($count > 1) {
      $form['config_container']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'config-fieldset-wrapper',
        ],
      ];
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the fields in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['config_container'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $count = $form_state->get('count');
    $add_button = $count + 1;
    $form_state->set('count', $add_button);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $count = $form_state->get('count');
    if ($count > 1) {
      $remove_button = $count - 1;
      $form_state->set('count', $remove_button);
    }

    $form_state->setRebuild();
  }

  /**
   * Final submit handler.
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('config_container');
    $values = $values['config_fieldset'];
    $form_state->set('count', count($values));

    $this->config('availability_check.additional_settings')
      ->set('values', serialize($values))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
